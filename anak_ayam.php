<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device width, initial-scale=1.0">
	<title>Keluarga Ayam</title>
	<link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">
	<style type="text/css">
		*{
			padding :0;
			margin :0;
			font-family: verdana;
		}
		body{
			background-image: url("ayam5.jpg");
			background-size: 100%; 
			text-align: center;
			display: flex;
			justify-content: center;
			align-items: center;
			flex-direction: column;
			height: 100vh;
			width: 100vw;
		}
		.container{
			height: 380px;
			width: 275px;
			background-color: rgb(0,0,0);
			border:solid cyan 2px;
			border-radius: 10px;
		}
		p{
			font-size: 18px;
			padding-bottom: 10px;
			color:white;
		}
		input{
			width: 200px;
			height: 25px;
			border: solid cyan 2px;
			border-radius: 15px;
			outline: none;
			background-color: black;
		}
		.a{
			color: white;
			margin-top: 5px;
			transition: 1px;
		}
		.b{
			margin-top: 5px;
			color: white;
			transition: 1px;
		}
		.b:hover{
			background-color: black;
			color: white;
		}
	</style>
</head>
<body>
	<div class="container">
		<br><img src="ayam2.jpg" width="225" height="180"></br>
		<form method="POST" action="proses_ayam.php" style="text-align: center;">
			<p>type the number of chickens you want?</p>
		<input type="text" name="anak_ayam" class="a" placeholder="input value...........">
		<input type="submit" value="Start" class="b">
		</form> 
	</div>
</body>
</html>